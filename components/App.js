export default ({ children }) => (
  <main>
    {children}
    <style jsx global>{`
      * {
        font-family: Verdana, Arial;
      }
      body {
        margin: 0;
        background: #ececec;
      }
      main {
        max-width: 600px;
        margin: 20px auto;
        position: relative;
        padding-bottom: 15px;
      }
    `}</style>
  </main>
)
