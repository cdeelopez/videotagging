import React, { Component } from 'react'
import { PropTypes } from 'prop-types'

export class Tag extends Component {
  constructor (props) {
    super(props)
  }

  render() {
    const { tag, onRemove } = this.props
    return (
      <div>
        {tag}
        <span onClick={onRemove}>x</span>
        <style jsx>{`
          div { background: #fff; padding: 6px 10px; margin-bottom: 1px; font-size: 12px; display: flex; justify-content: space-between; align-items: center; }
          span { width: 10px; font-size: 11px; cursor: pointer; display: block; padding: 4px; text-align: center; }
        `}</style>
      </div>
    )
  }
}

Tag.propTypes = {
  tag: PropTypes.string.isRequired,
  onRemove: PropTypes.func.isRequired
};

export default Tag;
