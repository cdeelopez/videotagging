
import React, { Component, Fragment } from 'react'
import fetch from 'isomorphic-fetch'
import Head from 'next/head'
import App from '../components/App'
import Tag from '../components/Tag'

export default class extends Component {

  static async getInitialProps({ req }) {
    const res = await fetch('https://dev-cerebro.video.aetnd.com/ultron/get-sorted-list')
    const images = await res.json()

    return { images }
  }

  onKeyPress = (event) => {
    if (event.key === 'Enter'){
      const tagList = this.state.tagList
      tagList.push(event.target.value)
      this.setState({ tagList })
      event.target.value = ''
     }
  }

  onSubmit = async(id) => {
    const isEnd = this.state.imageIndex + 1 < this.props.images.length ? false : true
    this.setState({ isSending: true, imageIndex: this.state.imageIndex + 1, isEnd: isEnd })
    const resp = await fetch('https://dev-cerebro.video.aetnd.com/ultron/create-labels', {
      method: 'POST',
      body: JSON.stringify({
        image_id: id,
        image_labels: this.state.tagList.join()
      })
    })
    this.setState({ isSending: false, tagList: [] })
  }

  onRemove = (tag) => {
    const tagList = this.state.tagList
    tagList.splice(tagList.indexOf(tag), 1)
    this.setState({ tagList })
  }

  constructor (props) {
    super(props)
    this.state = {
      imageIndex: 0,
      isSending: false,
      isEnd: false,
      tagList: []
    }
  }

  render () {
    const { images } = this.props
    const { tagList, isEnd, isSending } = this.state

    return (
      <App>
        { isEnd && <div>The End</div> }
        { isSending && <div className="sending">Sending...</div> }
        { !isEnd && <Fragment>
          <figure>
            <img src={images[this.state.imageIndex].image_url} />
          </figure>
          <section>
            <div className="input-wrapper">
              <small>What do you see?</small>
              <input type="text" name="tag" onKeyPress={this.onKeyPress} />
            </div>
            <div className="tag-list">
              { tagList.map( tag => <Tag key={tag} tag={tag} onRemove={() => this.onRemove(tag)}>{tag}</Tag> ) }
            </div>
            <button onClick={() => this.onSubmit(images[this.state.imageIndex].image_id)}>Submit</button>
          </section>
          </Fragment>
        }
        <style jsx>{`
          figure { margin: 0; padding: 5px; }
          img { width: 100%; }
          .input-wrapper { max-width: 240px; width: 90%; margin: 20px auto; position: relative; padding: 8px; border: 1px solid #ccc; }
          input { height: 36px; width: 100%; border:none; padding: 4px; font-size: 12px; box-sizing: border-box; }
          small { position: absolute; padding: 0 8px; background: #ececec; left: 12px; top: -10px; font-size: 11px; }
          button { border: none; background: #555; padding: 10px 20px; margin: 0 auto; text-transform: uppercase; color: rgba(255,255,255,0.9); max-width: 240px; width: 90%; display: block; cursor: pointer; }
          .tag-list { margin-bottom: 40px }
          .sending { position: absolute; top: 0; right: 0; bottom: 0; left: 0; background: rgba(255, 255, 255, 0.9); display: flex; justify-content: center; align-items: center; color: #666; z-index: 100; }
        `}</style>
      </App>
    )
  }
}
