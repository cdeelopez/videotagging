import Document, { Head, Main, NextScript } from 'next/document';

export default class CustomDocument extends Document {
  render() {
    return (
      <html lang='en-US'>
        <Head>
          <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=no" />
          <meta name='robots' content='noindex' />
          <meta httpEquiv='X-UA-Compatible' content='IE=edge' />
        </Head>

        <body>
          <Main />
          <NextScript/>
        </body>
      </html>
    );
  }
}
